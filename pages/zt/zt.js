

var app=getApp();
Page({
    data:{
         SkinStyle:"normal"      //这里其实可以不要
    }, 
    onLoad: function (options) {
      var that=this;
      wx.getStorage({
      key: 'skins',
      success: function(res) {
          that.setData({
              SkinStyle: res.data
          })
      },
    })
    },
    // 添加切换按钮的点击事件bgBtn：
    bgBtn:function(){
        if (this.data.SkinStyle==="normal"){
            app.globalData.skin = "dark";   //设置app（）中皮肤的类型,如果当前值等于白天就显示正常皮肤
            this.setData({
                SkinStyle: app.globalData.skin  //设置SkinStyle的值
            })
            wx.setStorage({         //设置storage
        　　 　　key: 'skins',
        　　 　　data: app.globalData.skin,
        　　 })
        }else{  //否则就是另一种皮肤黑夜
            app.globalData.skin="normal";
            this.setData({
                SkinStyle: "normal"
            })
            wx.setStorage({
        　　 　　key: 'skins',
        　　 　　data: app.globalData.skin,
        　　 })           
        }        
    }
})        
// 当然它不只是一个页面，其他每个页面，包括切换皮肤的当前的页面的Page中的onLoad事件里，读取storage并设置SkinStyle的值：id皮肤的值   当然前提是每个页面的view里面添加这个id
