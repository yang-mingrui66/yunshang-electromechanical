//获取应用实例
const app = getApp();

Page({
  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    menuitems: [
      // 导航栏分别跳转不同页面
      { text: '个人资料', url: '../grxx/grxx', icon:'/images/grzl.jpg', tips: '' },
      { text: '我的地址', url: '../address/address', icon:'/images/dz.jpg', tips: '' },
      { text: '帮助中心', url: '../bzzx/bzzx', icon:"/images/bzzx.jpg", tips: '' },
      { text: '设置', url: '../setup/setup', icon: '/images/sz.jpg', tips: '' }
    ]
  },
  
  // * 生命周期函数--监听页面加载
  //  */
  onLoad: function (options) {
    //读取storage并设置SkinStyle的值：皮肤的值
    var that = this;
    wx.getStorage({
    key: 'skins',
    success: function(res) {
        that.setData({
            SkinStyle: res.data
        })
    },
  })
    if (app.globalData.userInfo) {
      that.setUserInfo(app.globalData.userInfo);
    } else if (that.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        that.setUserInfo(res.userInfo);
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          that.setUserInfo(res.userInfo);
        }
      })
    }
  },

  getUserInfo: function (e) {
    this.setUserInfo(e.detail.userInfo);
  },

  setUserInfo: function (userInfo) {
    if (userInfo != null) {
      app.globalData.userInfo = userInfo
      this.setData({
        userInfo: userInfo,
        hasUserInfo: true
      })
    }
  }
})