Page({
  data: {
    phone: '',
    password:''
  },
  onLoad: function (options) {
    //读取storage并设置SkinStyle的值：皮肤的值
    var that = this;
    wx.getStorage({
    key: 'skins',
    success: function(res) {
        that.setData({
            SkinStyle: res.data
        })
    },
   })
  },
// 获取输入账号
  phoneInput :function (e) {
    this.setData({
      phone:e.detail.value
    })
  },
 
// 获取输入密码
  passwordInput :function (e) {
    this.setData({
      password:e.detail.value
    })
  },
 
// 登录
  login: function () {
    if(this.data.phone.length == 0 || this.data.password.length == 0){//如果用户名或者密码为0显示用户名或者密码不能为空
      wx.showToast({  
        title: '用户名和密码不能为空',  
        icon: 'loading',  
        duration: 2000  
      })  
}else {//否则就登录成功，当然这里只是前端方面的只做了部分，后端数据库就是其他成员做的了
  // 这里修改成跳转的页面
      wx.showToast({  
        title: '登录成功',  
        icon: 'success',  
        duration: 2000  
      })  
    }  
  }
})