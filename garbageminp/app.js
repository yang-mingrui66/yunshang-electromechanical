// app.js
App({
  onLaunch() {
   if(!wx.cloud){
     console.log('请使用2.2.4以上的基础库，进行云发')
   }else{
     wx.cloud.init({
       env:'cloud1-4gf2a5ysda4c29d2', //云环境
       traceUser:true
     })
   }
    // 展示本地存储能力
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
  },
  globalData: {
    userInfo: null,
    skin:"normal"
  }
})
